from typing import List

count = 0
def num_paths_recursive_helper(arr: List[List[int]], i: int, j: int) -> int:
    global count; count += 1
    if i == 0 and j == 0:
        return 0
    if i == 0 or j == 0:
        return 1
    return num_paths_recursive_helper(arr, i-1, j) + num_paths_recursive_helper(arr, i, j-1)

def num_paths_recursive(arr: List[List[int]]) -> int:
    return num_paths_recursive_helper(arr, len(arr)-1, len(arr[0])-1)


a = [[None for _ in range(10)] for _ in range(10)]
print(num_paths_recursive(a))
print(f'count: {count}')

memo_count = 0
cache = {}
def num_paths_memoized_helper(arr: List[List[int]], i: int, j: int) -> int:
    if (i, j) in cache:
        return cache[(i, j)]

    global memo_count; memo_count += 1

    if i == 0 and j == 0:
        cache[(i, j)] = 0
    elif i == 0 or j == 0:
        cache[(i, j)] = 1
    else:
        cache[(i, j)] = num_paths_memoized_helper(arr, i-1, j) + num_paths_memoized_helper(arr, i, j-1)
    return cache[(i, j)]

def num_paths_memoized(arr: List[List[int]]) -> int:
    return num_paths_memoized_helper(arr, len(arr)-1, len(arr[0])-1)


print(num_paths_memoized(a))
print(f'memo_count: {memo_count}')


def num_paths_dp(arr: List[List[any]]) -> int:
    rows = len(arr)
    cols = len(arr[0])
    np = [[None for _ in range(cols)] for _ in range(rows)]
    np[0][0] = 0
    for i in range(1, rows):
        np[i][0] = 1
    for j in range(1, cols):
        np[0][j] = 1
    for i in range(1, rows):
        for j in range(1, cols):
            np[i][j] = np[i-1][j] + np[i][j-1]
    return np[rows-1][cols-1]

print(num_paths_dp(a))
