def coin_change_recursive(s, coins):
    if s == 0:
        return 0
    elif s < 0:
        return None
    else:
        cc = [coin_change_recursive(s - c, coins) for c in coins]
        cc_filtered = [i for i in cc if i is not None]
        return 1 + min(cc_filtered) if cc_filtered else None


S = 53
coins = [1, 2, 5, 10, 100, 500]
# print(coin_change_recursive(S, coins))

cache = {}


def coin_change_memoized(s, coins):
    if s in cache:
        return cache[s]

    if s < 0:
        return None

    if s == 0:
        cache[s] = 0
    else:
        cc = [coin_change_memoized(s - c, coins) for c in coins]
        cc_filtered = [i for i in cc if i is not None]
        cache[s] = 1 + min(cc_filtered) if cc_filtered else None

    return cache[s]


print(coin_change_memoized(S, coins))

from math import inf


def coin_change_dp(s, coins):
    cc = [inf for _ in range(s + 1)]
    cc[0] = 0

    for i in range(1, s + 1):
        for c in coins:
            if c > s:
                continue
            cc[i] = min(cc[i], 1 + cc[i - c])
    return cc[s]


print(coin_change_dp(S, coins))