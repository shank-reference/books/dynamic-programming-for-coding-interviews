def I(n):
    result = []
    for i in range(n):
        row = [0 for _ in range(i)] + [1] + [0 for _ in range(n - i - 1)]
        result.append(row)
    return result


def matrix_mul(A, B):
    # A and B are matrices in the form of list of lists (2D array)
    if len(A) is 0 or len(B) is 0:
        raise ValueError("only non-empty matrices can be multiplied")

    if len(A[0]) != len(B):
        raise ValueError(
            "number of columns in A should be equal to number of rows in B")

    result = []
    a_rows = len(A)
    a_cols = len(A[0])
    b_cols = len(B[0])
    for r in range(a_rows):
        row = []
        for c in range(b_cols):
            val = sum(A[r][i] * B[i][c] for i in range(a_cols))
            row.append(val)
        result.append(row)
    return result


def to_binary(x):
    res = []
    while x > 0:
        if x % 2 is 1:
            res.append(1)
        else:
            res.append(0)
        x = x // 2
    result = 0
    res.reverse()
    for i in res:
        result = result * 10 + i
    return result


def matrix_power_naive(A, x):
    if x is 0:
        return I(len(A))

    result = A
    for _ in range(x - 1):
        result = matrix_mul(result, A)
    return result


def matrix_power_smart(A, x):
    result = I(len(A))
    x2 = to_binary(x)
    cur_A = A
    for i in str(x2)[::-1]:
        if i == "1":
            result = matrix_mul(result, cur_A)
        cur_A = matrix_mul(cur_A, cur_A)
    return result


def matrix_power_final(A, x):
    result = I(len(A))
    cur_A = A
    while x > 0:
        if x % 2 == 1:
            result = matrix_mul(result, cur_A)
        cur_A = matrix_mul(cur_A, cur_A)
        x = x // 2
    return result
