def is_interleaving_recursive(a: str, b: str, c: str) -> bool:
    # all strings are empty
    if not a and not b and not c:
        return True

    if not c:
        return False

    if not a and not b:
        return False

    first_a = False
    if a and c and a[0] == c[0]:    # first character of a = first character of c
        first_a = is_interleaving_recursive(a[1:], b, c[1:])

    first_b = False
    if b and c and b[0] == c[0]:    # first character of b = first character of c
        first_b = is_interleaving_recursive(a, b[1:], c[1:])

    return first_a or first_b

a = "bcc"
b = "bbca"
c = "bbcbcac"
print(is_interleaving_recursive(a, b, c))


def is_interleaving_dp(a: str, b: str, c: str) -> bool:
    x = len(a)
    y = len(b)

    ilv = [[None for _ in range(y+1)] for _ in range(x+1)]

    # check if the first a[:i] and b[:j] is interleaving with c[:i+j]
    ilv[0][0] = True

    for j in range(1, y+1):     # first row (b)
        ilv[0][j] = ilv[0][j-1] and b[j-1] == c[j-1]

    for i in range(1, x+1):     # first col (a)
        ilv[i][0] = ilv[i-1][0] and a[i-1] == c[i-1]

    # c[:i+j] is interleaving with a[:i] and b[:j] if
    # c[:i+j-1] is interleaving with either
    #   a[:i-1], b[:j] and a[i] == c[i+j]
    #   or
    #   a[:i], b[:j-1] and b[j] == c[i+j]
    for i in range(1, x+1):
        for j in range(1, y+1):
            last_a = ilv[i-1][j] and a[i-1] == c[i+j-1]
            last_b = ilv[i][j-1] and b[j-1] == c[i+j-1]
            ilv[i][j] = last_a or last_b

    return ilv[x][y]


print(is_interleaving_dp(a, b, c))
