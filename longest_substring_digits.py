def max_substring_length_brute_force(s):
    n = len(s)
    max_len = 0

    # i - starting index of substring
    for i in range(n):
        # j - end index of substring (even length)
        for j in range(i + 1, n, 2):
            # len - length of the current substring
            l = j - i + 1

            if max_len > l:
                continue

            lsum = 0
            rsum = 0
            for k in range(l // 2):
                lsum += int(s[i + k])
                rsum += int(s[i + l // 2 + k])

            if lsum == rsum:
                max_len = l

    return max_len


print(max_substring_length_brute_force("12332411111111111111111"))


def max_substring_length_dp(s):
    n = len(s)
    max_len = 0

    strsum = [[0] * n] * n
    # lower diagonal of the matrix is not used
    # filling diagonal values
    for i in range(n):
        strsum[i][i] = s[i]

    for l in range(2, n - 1):
        # pick i and j for the current substring
        for i in range(n - l + 1):
            j = i + len - 1
            k = i + len // 2
            strsum[i][j] = strsum[i][j - k] + strsum[j - k + 1][j]

            if len % 2 is 0 and strsum[i][j - k] == strsum[j - k + 1][
                    j] and l > max_len:
                max_len = l

    return max_len


print(max_substring_length_brute_force("12332411111111111111111"))