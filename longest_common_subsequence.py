def lcs_recursive(s1: str, s2: str) -> int:
    if not s1 or not s2:
        return 0

    elif s1[-1] == s2[-1]:
        return 1 + lcs_recursive(s1[:-1], s2[:-1])

    else:
        return max(lcs_recursive(s1[:-1], s2), lcs_recursive(s1, s2[:-1]))


A = "AAAAB"
B = "AAAACB"
print(lcs_recursive(A, B))

cache = {}


def lcs_memoized(s1 : str, s2: str) -> int:
    if (s1, s2) in cache:
        return cache[(s1, s2)]

    if not s1 or not s2:
        cache[(s1, s2)] = 0

    elif s1[-1] == s2[-1]:
        cache[(s1, s2)] = 1 + lcs_memoized(s1[:-1], s2[:-1])

    else:
        cache[(s1, s2)] = max(
            lcs_memoized(s1[:-1], s2), lcs_memoized(s1, s2[:-1]))

    return cache[(s1, s2)]


print(lcs_memoized(A, B))

def lcs_dp(s1: str, s2: str) -> int:
    m, n = len(s1), len(s2)
    lcs = [[None for _ in range(n+1)] for _ in range(m+1)]

    for r in range(m+1):
        lcs[r][0] = 0

    for c in range(n+1):
        lcs[0][c] = 0

    for r in range(1, m+1):
        for c in range(1, n+1):
            if s1[r-1] == s2[c-1]:
                lcs[r][c] = 1 + lcs[r-1][c-1]
            else:
                lcs[r][c] = max(lcs[r-1][c], lcs[r][c-1])

    return lcs[r][c]


print(lcs_dp(A, B))
