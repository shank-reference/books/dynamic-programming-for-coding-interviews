def min_path_cost(cost, m, n):
    if m is 0 and n is 0:
        return cost[0][0]

    if m is 0:
        return min_path_cost(cost, 0, n - 1) + cost[0][n]

    if n is 0:
        return min_path_cost(cost, m - 1, 0) + cost[m][0]

    x = min_path_cost(cost, m - 1, n)
    y = min_path_cost(cost, m, n - 1)
    return min(x, y) + cost[m][n]


M = 10  # no. of rows in the cost array
N = 20  # no. of cols in the cost array
cache = [[None] * N] * M


def min_path_cost_memoized(cost, m, n):
    if cache[m][n] is not None:
        return cache[m][n]

    if m is 0 and n is 0:
        cache[m][n] = cost[0][0]
        return cache[m][n]

    if m is 0:
        cache[m][n] = min_path_cost(cost, 0, n - 1) + cost[0][n]
        return cache[m][n]

    if n is 0:
        cache[m][n] = min_path_cost(cost, m - 1, 0) + cost[m][0]
        return cache[m][n]

    x = min_path_cost(cost, m - 1, n)
    y = min_path_cost(cost, m, n - 1)
    cache[m][n] = min(x, y) + cost[m][n]
    return cache[m][n]


min_cost = [[None] * N] * M


def min_path_cost_dp(cost, m, n):
    min_cost[0][0] = cost[0][0]

    # top row
    for c in range(n):
        min_cost[0][c] = min_cost[0][c - 1] + cost[0][c]

    # left column
    for r in range(m):
        min_cost[r][0] = min_cost[r - 1][0] + cost[r][0]

    # filling for other cells
    for r in range(1, m):
        for c in range(1, n):
            min_cost[r][c] = min(min_cost[r - 1][c],
                                 min_cost[r][c - 1]) + cost[m][n]

    return min_cost[m - 1][n - 1]
