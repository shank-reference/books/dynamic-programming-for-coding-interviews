def ways_to_score(n, pts):
    if n < 0:
        return 0
    if n == 0:
        return 1

    ways = 0
    for p in pts:
        ways += ways_to_score(n - p, pts)
    return ways


N = 10
cache = [None] * N


def ways_to_score_memoized(n, pts):
    if cache[n] is not None:
        return cache[n]

    if n < 0:
        return 0
    if n == 0:
        cache[0] = 1
        return cache[0]

    ways = 0
    for p in pts:
        ways += ways_to_score(n - p, pts)
    cache[n] = ways
    return cache[n]


ways = [0] * (N + 1)


def ways_to_score_dp(n, pts):
    ways[0] = 1
    for i in range(1, n + 1):
        for p in pts:
            if i - p > 0:
                ways[i] += ways[i - p]
    return ways[n]
