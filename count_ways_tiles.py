# count the number of ways of placing 2*1 tiles into an area of 2*n
def count_ways(n):
    if n == 0: return 0
    if n == 1: return 1
    if n == 2: return 2
    return count_ways(n - 1) + count_ways(n - 2)
