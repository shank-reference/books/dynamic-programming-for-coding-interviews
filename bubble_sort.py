def bubble_sort_iterative(lst):
    n = len(lst)
    for i in range(n-1):
        for j in range(n-i-1):
            if lst[j] > lst[j+1]:
                lst[j], lst[j+1] = lst[j+1], lst[j]
    print(lst)
    return lst

bubble_sort_iterative([1, 10, 5, 6, 7, 8])


def bubble_sort_recursive(lst):
    bubble_sort_recursive_helper(lst, len(lst))

def bubble_sort_recursive_helper(lst, cnt):
    if cnt is 1:
        print(lst)
        return lst
    for j in range(cnt-1):
        if lst[j] > lst[j+1]:
            lst[j], lst[j+1] = lst[j+1], lst[j]
    bubble_sort_recursive_helper(lst, cnt-1)

bubble_sort_recursive([1, 10, 5, 6, 7, 8])
