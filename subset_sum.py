from typing import List


def subset_sum_recursive(arr: List[int], x: int) -> bool:
    if x < 0:
        return False

    if x == 0:
        return True

    if not arr:
        return False

    return subset_sum_recursive(arr[1:], x-arr[0]) or subset_sum_recursive(arr[1:], x)


L = [3, 2, 7, 1]
X = 10
print(subset_sum_recursive(L, X))


def subset_sum_dp(arr: List[int], x: int) -> bool:
    m = len(arr)
    ss = [[None for _ in range(x + 1)] for _ in range(m)]

    # ss[i][j] is true if there is a subset of arr[0..i] with sum equal to j, otherwise false

    for r in range(m):
        ss[r][0] = True

    for c in range(1, x + 1):
        ss[0][c] = True if c == arr[0] else False

    for r in range(1, m):
        for c in range(1, x + 1):
            v = arr[r]
            if v == c:
                ss[r][c] = True
            elif ss[r - 1][c] is True:
                ss[r][c] = True
            elif c >= v:
                ss[r][c] = ss[r - 1][c - v]
            else:
                ss[r][c] = False

    return ss[m - 1][x]


print(subset_sum_dp(L, X))
