def lps_recursive(s: str) -> int:
    if not s:
        return 0

    if len(s) == 1:
        return 1

    if s[0] == s[-1]:
        return 2 + lps_recursive(s[1:-1])
    else:
        return max(lps_recursive(s[1:]), lps_recursive(s[:-1]))


S = "testing"
print(lps_recursive(S))
