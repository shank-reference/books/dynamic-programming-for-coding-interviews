class Node:
    def __init__(self, value):
        self.val = value
        self.left = None
        self.right = None


def add_child_sum(root):
    # add child sums for the left child
    # add child sums for the right child
    # calculate the value for this node
    if root is None:
        return

    add_child_sum(root.left)
    add_child_sum(root.right)

    final_sum = root.val
    if root.left is not None:
        final_sum += root.left.val
    if root.right is not None:
        final_sum += root.right.val

    root.val = final_sum
