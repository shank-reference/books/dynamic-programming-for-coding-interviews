from typing import List
from math import inf

def max_subarray_sum_bf(arr: List[int]) -> int:
    if all(x < 0 for x in arr):
        return max(arr)

    max_sum = 0
    n = len(arr)

    for i in range(n):
        for j in range(i, n):
            s = sum(arr[i:j + 1])
            max_sum = max(s, max_sum)

    return max_sum


a = [-2, -3, 4, -1, -2, 1, 5, -3]
print(max_subarray_sum_bf(a))


# divide and conquer
def max_subarray_sum_dnc(arr: List[int]) -> int:
    return max_subarray_sum_dnc_helper(arr, 0, len(arr)-1)

def max_subarray_sum_dnc_helper(arr: List[int], l: int, h: int) -> int:
    """
    l and h are the indices of the elements (inclusive) for which the max
    subarray sum is to be calculated
    """
    # base case: all negative integers
    if all(x < 0 for x in arr[l:h+1]):
        return max(arr)

    # base case: only one element
    if l == h:
        return arr[l]

    m = (l + h) // 2    # middle point
    return max(
        max_subarray_sum_dnc_helper(arr, l, m),
        max_subarray_sum_dnc_helper(arr, m+1, h),
        max_crossing_sum(arr, l, m, h)
    )

def max_crossing_sum(arr: List[int], l: int, m: int, h: int) -> int:
    s: int = 0
    left_sum: int = -inf
    for i in range(m, l, -1):
        s += arr[i]
        left_sum = max(left_sum, s)

    s: int = 0
    right_sum: int = -inf
    for i in range(m+1, h+1):
        s += arr[i]
        right_sum = max(right_sum, s)

    return left_sum + right_sum

print(max_subarray_sum_dnc(a))

def max_subarray_sum_dp(arr: List[int]) -> int:
    max_so_far = 0
    max_ending_here = -inf
    for elem in arr:
        max_ending_here = max(0, max_ending_here + elem)
        max_so_far = max(max_so_far, max_ending_here)
    return max_so_far

print(max_subarray_sum_dp(a))
